package testcases;

import base.BaseSetup;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.SignInPage;

public class SignInTest extends BaseSetup {
    private WebDriver driver;
    public SignInPage SignInPage;

    @BeforeMethod
    public void setUp() {
        driver = getDriver();
    }


    @Test(dataProvider = "data_signin")
    public void signIn(String Username, String Password) throws Exception{
        System.out.println(driver);
        SignInPage = new SignInPage(driver);
        Assert.assertTrue(SignInPage.verifySignInPageTitle(),"Sign In page title doesn't match");
        SignInPage.signin(Username,Password);
    }
}

package resources.data;

import org.testng.annotations.DataProvider;

public class signInData {
    @DataProvider(name ="data_signin")
    public Object[][] dpMethod() {
        return new Object[][]{
                {"Tuan1","abc"},
                {"Admin","admin123"},
                {"Tuan","abc"},
        };
    }
}
